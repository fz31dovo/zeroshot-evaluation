import pandas as pd
import mdbh

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

db_name = ""

uri = mdbh.get_mongodb(".mongo.conf", db_name)
df = mdbh.get_dataframe(uri)
# df = pd.read_pickle("zeroshot_results.pkl")

df = df[df["id"] > 50]
df = df[["experiment.name", 'metrics.test_accuracy',
         'metrics.test_multilabel_report_micro avg_f1-score', 'metrics.test_multilabel_report_micro avg_precision',
         'metrics.test_multilabel_report_micro avg_recall', 'metrics.test_multilabel_report_macro avg_f1-score',
         'metrics.test_multilabel_report_macro avg_precision', 'metrics.test_multilabel_report_macro avg_recall',
         "metrics.test_p@1", "metrics.test_p@3", "metrics.test_p@5", "start_time", "stop_time", "config.batch_size",
         "config.dataset", "config.representation", "config.threshold", "config.target", "config.formatted",
         "config.method", "config.whole_dataset", "config.cut_sample", "config.dataset_size"]]

mapping = {
    "experiment.name": "name",
    "config.batch_size": "batch_size",
    "config.dataset": "dataset",
    "config.threshold": "threshold",
    "config.target": "target",
    "config.formatted": "formatted",
    "config.method": "method",
    "config.whole_dataset": "whole_dataset",
    "config.cut_sample": "cut_sample",
    "config.dataset_size": "dataset_size",
    "metrics.test_accuracy": "accuracy",
    "metrics.test_multilabel_report_micro avg_f1-score": "micro_f1",
    "metrics.test_multilabel_report_micro avg_precision": "micro_precision",
    "metrics.test_multilabel_report_micro avg_recall": "micro_recall",
    "metrics.test_multilabel_report_macro avg_f1-score": "macro_f1",
    "metrics.test_multilabel_report_macro avg_precision": "macro_precision",
    "metrics.test_multilabel_report_macro avg_recall": "macro_recall",
    "metrics.test_p@1": "precision@1",
    "metrics.test_p@3": "precision@3",
    "metrics.test_p@5": "precision@5",
    "config.representation": "representation"
}

df = df.rename(columns=mapping)

# Calculate duration of experiments
time_list = []
for index, row in df.iterrows():
    time = row['stop_time'] - row['start_time']
    time_list.append(time)
df["duration"] = time_list
df = df.drop(columns=["stop_time", "start_time"])

df = df.sort_values('name')
df["duration"] = df["duration"].map(lambda x: str(x).split(" ")[-1][:8])
df.loc[df["cut_sample"] != True, "cut_sample"] = False

df.to_pickle("./zeroshot_results_formatted.pkl")