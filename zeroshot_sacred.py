from zeroshot import ex
import yaml

with open("config.yaml", "r") as f:
    config = yaml.load(f, Loader=yaml.SafeLoader)

datasets = config["datasets"]

single = ["wiki30k", "20newsgroup", "agnews", "dbpedia", "yahoo_answers", "movie_reviews", "trec6", "trec50", "yelpfull", "amazonfull"]
multi = ["rcv1", "blurbgenrecollection", "blurbgenrecollection_de", "ohsumed",]

for dataset in datasets:
    if dataset in single:
        target = "single"
        threshold = "max"
    elif dataset in multi:
        target = "multi"
        threshold = config["multi_threshold"]

    experiment = ex.run(options={'--name': f'{dataset}-{config["representation"]}'},
                        config_updates={'device': config["device"],
                                        "batch_size": config["batch_size"],
                                        'representation': config["representation"],
                                        "target": target,
                                        'threshold': threshold,
                                        "formatted": config["formatted"],
                                        "cut_sample": config["cut_sample"],
                                        'method': config["method"],
                                        "whole_dataset": config["whole_dataset"],
                                        'dataset': dataset})